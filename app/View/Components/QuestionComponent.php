<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Question;
use App\Answer;
use Auth;
use App\User;

class QuestionComponent extends Component
{
    public $question;
    public $index;
    public $last;
    public $total;
    public $propertyId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($question, $index, $last, $total, $propertyId)
    {
        $this->question     = $question;
        $this->index        = $index;
        $this->last         = $last;
        $this->total        = $total;
        $this->propertyId   = $propertyId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $term     = session('term');
        $question = Question::find($this->question->id);
        $answers  = Question::getAnswersByUserId($question, $this->propertyId, $term);
        $users    = User::where('status', 'a')->get();

        return view('components.question-component', [
            'question'  => $this->question,
            'index'     => $this->index,
            'last'      => $this->last,
            'total'     => $this->total,
            'answers'   => $answers,
            'users'     => $users
        ]);
    }
}

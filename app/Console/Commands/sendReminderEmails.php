<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Answer;
use App\Mail\ReminderEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;

class sendReminderEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email notification to users about reminders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reminders = Answer::with(['users', 'question', 'property', 'term'])->where('reminder', '!=', '')->where('reminder', '=', date('Y-m-d'))->get();

        $data      = [];

        if( !empty( $reminders ) ){

            foreach ($reminders as $reminder) {
                $data[$reminder->question_id] = $reminder;
            }

            foreach( $data as $question_id => $reminder ){
                $this->sendEmailToUsers( $reminder );    
            }
        }   
    }

    private function sendEmailToUsers( $reminders ){ 

        $users      = [];
        $usernames  = '';

        if( !empty($reminders->users )){
            foreach ($reminders->users as $key => $value) {
                array_push($users, $value['first_name']);
            }
            $usernames = implode( ', ', $users );

            Mail::to($reminders->users)->send(new ReminderEmail( $reminders, $usernames ));
        }

        // Mail::to($user['email'])->send(new ReminderEmail($reminders));
    }
}

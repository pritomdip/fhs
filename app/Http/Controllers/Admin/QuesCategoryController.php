<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\QuesCategory;

class QuesCategoryController extends Controller
{
     public function __construct(){
    	$this->middleware('auth:admin');
    }

    public function index(){

    	$categories = QuesCategory::orderby('id', 'desc')->paginate(10);
		return view('admin.category.list-category', compact('categories'));

    }

    public function show(){

		return view('admin.category.add-category');
    }

    public function store(Request $request){

    	$this->rules($request);
    	QuesCategory::create($request->all());

   	   return redirect('/admin/category/list')->with('message', "Category created Succesfully");
    }

    public function edit($id){
    	$category 		= QuesCategory::find($id);

    	return view('admin.category.edit-category', [
    		'category'	=> $category
    	] );
    }

    public function update(Request $request, $id){

    	$category = QuesCategory::find($id);
    	$data 	  = $request->all();

    	$this->rules($request);
    	$category->update($data);

    	return redirect("/admin/category/edit/$id")->with('message', 'Category updated successfully!');
    }

    public function destroy($id){

        $property 	  = QuesCategory::findOrFail($id);
        $property->delete(); 

        return redirect('/admin/category/list')->with('message', 'Category Deleted successfully');
    }

    public function rules($request){

    	return $request->validate([
    		'cat_name'		=> 'required',
    	]);
    }
}

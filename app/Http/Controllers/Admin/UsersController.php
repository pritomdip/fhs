<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct(){
    	$this->middleware('auth:admin');
    }

    public function index(){

    	$users = User::orderby('id', 'desc')->paginate(10);
		return view('admin.users.list-users', compact('users'));
    }

    public function show(){

		return view('admin.users.add-user');
    }

    public function store(Request $request){

    	$this->rules($request);

        $request->password = Hash::make($request->password);

		User::create([
			'first_name'=> $request->first_name,
            'last_name' => $request->last_name,
			'email'		=> $request->email,
			'password'	=> $request->password,
			'status'	=> $request->status
		]);

   	   return redirect('/admin/users/list')->with('message', "User created Succesfully");
    }

    public function edit($id){
    	$user = User::find($id);
    	return view('admin.users.edit-user', compact('user') );
    }

    public function update(Request $request, $id){

    	$user = User::find($id);
    	$data = $request->all();

        $request->validate([
            'email'     => 'email|required',
            'first_name'=> 'required',
            'last_name' => 'required',
            'status'    => 'required',
            'password'  => 'required|min:8'
        ]);

        if( strlen($request->password ) == 60 && preg_match('/^\$2y\$/', $request->password ) ){
            $data['password'] = $request->password;
        } else {
            $data['password'] = Hash::make($request->password);
        }

    	$user->update($data);

    	return redirect("/admin/users/edit/$id")->with('message', 'User updated successfully!');
    }

    public function rules($request){

    	return $request->validate([
    		'email'		=> 'email|required',
    		'first_name'=> 'required',
            'last_name' => 'required',
    		'password'	=> 'min:8|required',
    		'status'	=> 'required'
    	]);
    }

    public function destroy($id){

        $users = User::findOrFail($id);
        $users->delete(); 
        return redirect('/admin/users/list')->with('message', 'Deleted successfully');
    }
}

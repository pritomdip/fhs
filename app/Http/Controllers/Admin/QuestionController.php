<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\QuesCategory;
use App\Question;
use App\Property;
use App\Term;

class QuestionController extends Controller
{
     public function __construct(){
    	$this->middleware('auth:admin');
    }

    public function index(){

    	$questions  = Question::orderby('id', 'desc')->paginate(10);  	

		return view('admin.question.list-question', compact('questions'));

    }

    public function show(){

    	$categories = QuesCategory::where('cat_status', 'a')->get();
    	$properties = Property::where('property_status', 'a')->get();  	   	
        $terms      = Term::all();       
    	
		return view('admin.question.add-question', compact('categories', 'properties', 'terms'));
    }

    public function store(Request $request){

    	$this->rules($request);

    	$question   = Question::create($request->all());
        $properties = Property::find($request->property_id);
        $terms      = Term::find($request->term_id);

        $this->saveTermData($terms, $question);
        $this->savePropertiesData($properties, $question);  

   	   return redirect('/admin/question/list')->with('message', "Question created Succesfully");
    }

    public function edit($id){
    	$question   = Question::find($id);
    	$categories = QuesCategory::where('cat_status', 'a')->get();
    	$properties = Property::where('property_status', 'a')->get();
        $terms      = Term::all();

    	return view('admin.question.edit-question', compact('question', 'categories', 'properties', 'terms') );
    }

    public function update(Request $request, $id){

    	$question = Question::find($id);
    	$data 	  = $request->all();
    	$this->rules($request);
    	$question->update($data);

        $question->properties()->detach();
        $properties = Property::find($request->property_id);

        $this->savePropertiesData( $properties, $question );

        $question->terms()->detach();
        $terms = Term::find( $request->term_id );

        $this->saveTermData( $terms, $question );

    	return redirect("/admin/question/edit/$id")->with('message', 'Question updated successfully!');
    }

    public function destroy($id){

        $property = Question::findOrFail($id);
        $property->delete(); 

        return redirect('/admin/question/list')->with('message', 'Question Deleted successfully');
    }

    public function rules($request){

    	return $request->validate([
    		'ques_name'		=> 'required',
            'term_id'       => 'required',
            'property_id'   => 'required',
            'category_id'   => 'required'
    	]);
    }

    private function saveTermData( $terms, $question ){
        if(!empty( $terms )){
            foreach ($terms as $term) {
                $question->terms()->attach($term);
            }
        }
        return true;
    }

    private function savePropertiesData( $properties, $question ){
        if( !empty( $properties )){
            foreach ($properties as $property) {
                $question->properties()->attach($property);
            }
        }
        return true;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\PropertyDesc;

class PropertyController extends Controller
{
    public function __construct(){
    	$this->middleware('auth:admin');
    }

    public function index(){

    	$properties = Property::orderby('id', 'desc')->paginate(10);
        return view('admin.property.list-property', compact('properties'));
    }

    public function show(){

		return view('admin.property.add-property');
    }

    public function store(Request $request){

    	$this->rules($request);

    	if( empty($request->property_slug )){
    		$request->property_slug = strtolower(preg_replace('/\s+/', '-', $request->property_name));
    	}

    	$property = Property::create([
    		'property_name'   => $request->property_name,
    		'property_slug'   => $request->property_slug,
    		'property_status' => $request->property_status,
    	]);

    	$id = $property->id;

    	if( !empty($id) && $id ){

			if ($request->hasFile('logo')) { 
		        $image = $request->file('logo'); 
		        $name = time().'.'.$image->getClientOriginalExtension();
		        $destinationPath = public_path('admin_assets/assets/image');
		        $image->move($destinationPath, $name);
		        $request->logo = $name;
		    }    		

    		PropertyDesc::create([
    			'property_id'	=> $id,
    			'logo'			=> $request->logo,
                'company_name'  => $request->company_name,
    			'address'		=> $request->address,
                'address_two'   => $request->address_two,
    			'city'			=> $request->city,
    			'postcode'		=> $request->postcode,
    			'country'		=> $request->country,
    			'manager'		=> $request->manager,
    			'email'			=> $request->email,
    			'tel'			=> $request->tel,
    		]);
    	}

   	   return redirect('/admin/property/list')->with('message', "Property created Succesfully");
    }

    public function edit($id){
    	$property 		= Property::find($id);
    	$property_desc  = $property->getPropertyDesc;

    	return view('admin.property.edit-property', [
    		'property'	=> $property,
    		'desc'		=> $property_desc
    	] );
    }

    public function update(Request $request, $id){

    	$property = Property::find($id);
    	$data = $request->all();

        if( empty($request->property_slug )){
    		$data['property_slug'] = strtolower(preg_replace('/\s+/', '-', $request->property_name));
    	}

    	$request->validate([
        	'property_name'	=> 'required'
        ]);
        
        $property = Property::where('id', $id)->update([
    		'property_name'   => $request->property_name,
    		'property_slug'   => $request->property_slug,
    		'property_status' => $request->property_status,
    	]);


    	if ($request->hasFile('logo')) { 

	        $image = $request->file('logo'); 
	        $name = time().'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('admin_assets/assets/image');
	        $image->move($destinationPath, $name);
	        $request->logo = $name;

	        $updateDesc = PropertyDesc::where('property_id', $id)->update([
				'logo'			=> $request->logo,
				'company_name'  => $request->company_name,
                'address'       => $request->address,
                'address_two'   => $request->address_two,
				'city'			=> $request->city,
				'postcode'		=> $request->postcode,
				'country'		=> $request->country,
				'manager'		=> $request->manager,
				'email'			=> $request->email,
				'tel'			=> $request->tel,
	    	]);

	    } else {

	    	$updateDesc = PropertyDesc::where('property_id', $id)->update([
				'company_name'  => $request->company_name,
                'address'       => $request->address,
                'address_two'   => $request->address_two,
				'city'			=> $request->city,
				'postcode'		=> $request->postcode,
				'country'		=> $request->country,
				'manager'		=> $request->manager,
				'email'			=> $request->email,
				'tel'			=> $request->tel,
	    	]);
	    }
    	
    	return redirect("/admin/property/edit/$id")->with('message', 'Property updated successfully!');
    }

    public function destroy($id){

        $property 	  = Property::findOrFail($id);
        $property->delete(); 
        $propertyDesc = PropertyDesc::where('property_id', $id)->delete(); 

        return redirect('/admin/property/list')->with('message', 'Property Deleted successfully');
    }

    public function rules($request){

    	return $request->validate([
    		'property_name'		=> 'required',
    		'logo' 				=> 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'manager'           => 'required',
            'email'             => 'required|email'
    	]);
    }
}

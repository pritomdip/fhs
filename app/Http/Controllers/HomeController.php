<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Image;
use Auth;
use App\Question;
use App\Term;
use Illuminate\Support\Facades\Storage;
use Session;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(){
        $terms = Term::all();

        return view('index', compact('terms'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($lang, Term $term)
    {
        Session::put('term', $term->id);

        $term       = session('term');
        $properties = Property::where('property_status', 'a')->get();
        return view('properties', compact('properties', 'term'));
    }

    public function propertyQuestions($lang, $id)
    {   
        $property    = Property::find($id);
        $term        = session('term');

        //Get all question by property and groupby category name
        $allQues     = Property::groupByQues( $id, $term ); 
        $grouped     = $allQues->groupBy('cat_name');
        $answer      = Property::groupByAnswer( $id, $term ); 

        $totalQues   = $allQues->count();

        $groupByAnswer = $answer->groupBy('answer');

        $score      = $this->propertyScore($groupByAnswer, $totalQues);

        return view('property', [
            'groupQuestions'    => $grouped,
            'property'          => $property,
            'term'              => $term,
            'score'             => $score
        ]);

    }

    public function questions( $lang, $id ){
        $term        = session('term');
        $property    = Property::find($id);
        $questions   = Property::groupByQues( $id, $term );
        
        if( isset($_GET['question']) && !empty($_GET['question'] )){
            $value = intval( $_GET['question'] );
        } else {
            $value = '';
        }

        return view('questions', compact('questions', 'property', 'value'));
    }

    public function answers($lang, Property $property, Request $request){

        $data    = $this->validation(request());
        $allData = request()->all();
        $user_id = Auth::id();
        $term_id = session('term');

        if( $allData['submit'] == 'Submit' ){
            $status = 'a';
        } else {
            $status = 'b';
        }

        // dd($status);

        if( !empty($data['question_id'] )){
            foreach($data['question_id'] as $key => $value){
                $answerKey          = $key + 1;
                $answer             = $property->answers()->updateOrCreate([
                    'question_id'   => $data['question_id'][$key],
                    'term_id'       => $term_id,
                    ], [ 
                    'user_id'       => $user_id,
                    'term_id'       => $term_id,   
                    'question_id'   => $data['question_id'][$key],
                    'answer'        => $data['answer'][$answerKey],
                    'remarks'       => $allData['remarks'][$key],
                    'priority'      => $allData['priority'][$key],
                    'deadline'      => $allData['deadline'][$key],
                    'reminder'      => $allData['reminder'][$key],
                    'status'        => $status
                ]);

                if( $answer ){
                    if( !empty($allData['image'][$answerKey] ) ){
                        foreach ($allData['image'][$answerKey] as $index => $value) {
                            if( !empty( $value['image'] )){

                                foreach ($value as $i => $item) {

                                    $setImage     = "image." . $answerKey . '.' . $index . ".image";

                                    if($request->hasFile($setImage)){ 
                                        // dd($setImage);
                                        $fileName = Question::saveImage($item,$request->file($setImage), $index, $data['question_id'][$key]);
                                    }
                                }

                                $answer->images()->create([
                                    'image' => $fileName
                                ]); 
                            }
                        }
                    }

                    if( !empty($allData['users'][$answerKey] ) ){
                        $answer->users()->detach();
                        foreach ($allData['users'][$answerKey] as $index => $value) {
                            if( !empty( $value['user_id'] )){
                                $answer->users()->attach($value);
                            }
                        }
                    }
                }
            }
        }

        return redirect()->back()->with('status', 'Answers successfully submitted');
    }

    public function deleteImage($lang, Property $property, Question $question, Request $request){

        $image = Image::where('image', $request->image_url)->delete();

        if( $image ){
            Storage::delete('/public/images/' . $request->image_url);
        }

        return response()->json(['success'=>'Evidence Deleted.']);
    }

    protected function validation($request){
        return $request->validate([
            'question_id.*'     => 'required',
            'answer.*'          => 'required',
            'image.*.*.image'   => 'mimes:jpeg,png,jpg,zip,pdf,ppt,pptx,xlx,xlsx,docx,doc'
        ]);
    }

    protected function propertyScore($answer, $total){

        $positive1 = $this->checkIfEmpty($answer, 'Yes' );
        $positive2 = $this->checkIfEmpty($answer, 'N/A' );
        $negative  = $this->checkIfEmpty($answer, 'No' );

        $allPositive = $positive1 + $positive2;

        if($total){
            $score  = round((100/$total) * $allPositive);
        } else {
            $score = 0;
        }
        return $score;
    }

    private function checkIfEmpty($answer, $value){
        if(!empty($answer) && isset($answer[$value])){
            $newValue = $answer[$value]->count();
        } else{
            $newValue = 0;
        }
        return $newValue;
    }
}

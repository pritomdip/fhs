<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.admin');
    }

    public function adminProfile(Admin $admin){
        if(Auth::id() == $admin->id){
            return view('admin.profile', compact('admin'));
        }
        return redirect()->back()->with('message', 'You are not authorized');
    }

    public function changePassword(Request $request, Admin $admin){
        if($request->password != $request->confirm_password){
            return redirect()->back()->with('error_msg', 'Password not matched');
        }

        $admin->password = bcrypt($request->password);

        $admin->save();

        return redirect()->back()->with('success', 'Password changed successfully');
    }
}

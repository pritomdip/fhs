<?php

namespace App\Http\Controllers\Auth\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{

    protected $redirectTo = '/admin.dashboard';


    public function __construct()
    {
        $this->middleware('guest:admin')->except('adminLogout');
    }

    public function showLoginForm(){

        return view('auth.admin.admin-login');
    }

    public function login(Request $request){
        
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $credentials = [
            'email'     => $request->email,
            'password'  => $request->password
        ];

        $logic = Auth::guard('admin')->attempt($credentials, $request->remember);
        if($logic){
            return redirect()->intended(route('admin.dashboard'));
        }
        return back()->withInput(['email', 'remember']);
    }

    public function adminLogout(){
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
}

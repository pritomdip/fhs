<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuesCategory extends Model
{
    protected $fillable = [
    	'cat_name', 'cat_status'
    ];

    public function categoryQuestions(){
    	return $this->hasMany('App\Question', 'category_id');
    }
}

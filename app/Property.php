<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Property extends Model
{
    protected $fillable = [
    	'property_name', 'property_slug', 'property_status'
    ];

    public function getPropertyDesc(){
    	return $this->hasOne('App\PropertyDesc', 'property_id');
    }

    public function questions(){
	    return $this->belongsToMany('App\Question', 'property_question', 'property_id', 'question_id');
	}

	public static function groupByQues($id, $term_id){
		$allQues =  DB::table('property_question')
	                ->where('property_question.property_id', '=', $id )
	                ->join('questions', 'property_question.question_id', '=', 'questions.id')
	                ->join('ques_categories', 'questions.category_id', '=', 'ques_categories.id' )
	                ->join('question_term', 'question_term.question_id', '=', 'questions.id')
	                ->where('question_term.term_id', $term_id)
	                ->select('ques_categories.*', 'questions.*')
	                ->where('ques_categories.cat_status', '=', 'a')
	                ->get();
	    
	    return $allQues;
	}

	public function answers(){
	    return $this->hasMany('App\Answer');
	}

	public static function groupByAnswer($id, $term_id){
		$answer =  DB::table('property_question')
	                ->where('property_question.property_id', '=', $id )
	                ->join('questions', 'property_question.question_id', '=', 'questions.id')
	                ->join('ques_categories', 'questions.category_id', '=', 'ques_categories.id' )
	                ->join('question_term', 'question_term.question_id', '=', 'questions.id')
	                ->join('answers', 'answers.question_id', '=', 'questions.id')
	                ->where('answers.property_id', $id)
	                ->where('answers.term_id', $term_id)
	                ->where('question_term.term_id', $term_id)
	                ->select('ques_categories.*', 'questions.*', 'answers.*')
	                ->where('ques_categories.cat_status', '=', 'a')
	                ->get();
	    
	    return $answer;
	}

	public function getUpdatedAtAttribute($date){
	    return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
	}

	public function getScore($term){
		$questions = self::groupByQues($this->id, $term)->count();
		$answers   = self::groupByAnswer($this->id, $term);

		$groupByAnswer = $answers->groupBy('answer');

		$positive1 = $this->checkIfEmpty($groupByAnswer, 'Yes' );
        $positive2 = $this->checkIfEmpty($groupByAnswer, 'N/A' );
        $negative  = $this->checkIfEmpty($groupByAnswer, 'No' );

        $allPositive = $positive1 + $positive2;

        if($questions){
            $score  = round((100/$questions) * $allPositive);
        } else {
            $score = 0;
        }
        return $score;

	}

	private function checkIfEmpty($answer, $value){
        if(!empty($answer) && isset($answer[$value])){
            $newValue = $answer[$value]->count();
        } else{
            $newValue = 0;
        }
        return $newValue;
    }
}

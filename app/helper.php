<?php

use App\Property;
use App\Question;

function abc($ques_id, $property_id, $term){
	$question = Question::find($ques_id);
    $answers  = Question::getAnswersByUserId($question, $property_id, $term);
    return $answers;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
    	'answer_id', 'image'
    ];

    public function question(){
    	return $this->belongsTo('App\Answer');
    }
}

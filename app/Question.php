<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\Storage;

class Question extends Model
{
    protected $fillable = [
    	'category_id', 'ques_name', 'ques_desc'
    ];

    public function getProperty(){
    	return $this->belongsTo('App\Property', 'property_id');
    }

    public function getCategory(){
    	return $this->belongsTo('App\QuesCategory', 'category_id');
    }

    public function properties(){
	    return $this->belongsToMany('App\Property', 'property_question', 'question_id', 'property_id');
	}

    public function answers(){
        return $this->hasMany('App\Answer');
    }

    public function terms(){
        return $this->belongsToMany('App\Term');
    }

    public static function saveImage($item, $image, $key, $question_id){
        $extension = $item->getClientOriginalExtension();
        $fileName  =  time() . '-' . Auth::id() . '-' . $key . $question_id . '.' . $extension;
        $image->storeAs('images', $fileName,'public');
        return $fileName;
    }

    public static function getAnswersByUserId($question, $property_id, $term){
        // dd($question->answers()->where('user_id', Auth::id())
        //             ->where('property_id', $property_id)
        //             ->where('term_id', $term)
        //             ->first());
        return $question->answers
                ->where('property_id', $property_id)
                ->where('term_id', $term)
                ->first();
    }

}

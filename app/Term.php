<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['term_name', 'term_status'];

    public function questions(){
    	return $this->belongsToMany('App\Question');
    }

    public function answers(){
    	return $this->hasMany('App\Answer');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Property;

class PropertyDesc extends Model
{
    protected $fillable = [
    	'property_id', 'logo', 'address', 'city', 'postcode', 'country', 'manager', 'email', 'tel', 'address_two', 'company_name'
    ];

    public function property()
 	{
        return $this->belongsTo('App\Property', 'property_id');
 	}
}

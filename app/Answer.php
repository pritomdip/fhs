<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Answer extends Model
{
    protected $fillable = [
    	'question_id', 'property_id', 'user_id', 'term_id', 'answer', 'remarks', 'priority', 'deadline', 'reminder', 'status'
    ];

    public function images(){
    	return $this->hasMany('App\Image');
    }

    public function property(){
    	return $this->belongsTo('App\Property');
    }

    public function setDeadlineAttribute($value)
	{
        if(!empty($value)){
            $this->attributes['deadline'] =  Carbon::parse($value);
        }   
	}

	public function setReminderAttribute($value)
	{
        if(!empty($value)){
            $this->attributes['reminder'] =  Carbon::parse($value);
        }
	}

    public function question(){
        return $this->belongsTo('App\Question');
    }

    public function getDeadlineAttribute($value)
    {
        if(!empty($value)){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getReminderAttribute($value)
    {
        if(!empty($value)){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function term(){
        return $this->belongsTo('App\Term');
    }
}

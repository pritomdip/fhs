<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $reminders;
    public $usernames;

    public function __construct($reminders, $usernames)
    {
        $this->reminders = $reminders;
        $this->usernames = $usernames;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.reminder', [
            'reminders' => $this->reminders,
            'usernames' => $this->usernames
        ]);
    }
}

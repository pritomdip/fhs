@extends('layouts.app')

@section('language')

<div class="language-switcher-wrapper">
    <a href="{{route(Route::currentRouteName(), ['language' => 'en', 'id' => $property->id])}}"><img src="{{asset('assets/images/en.png')}}" class="fr"></a>
    <a href="{{route(Route::currentRouteName(), ['language' => 'nl', 'id' => $property->id])}}"><img src="{{asset('assets/images/nl.png')}}" class="en"></a>
</div>

@endsection

@section('content')

<div class="bs-example" data-example-id="striped-table" style="padding:20px;">

    <div class="property-info-wrapper" style="padding-bottom:30px;">
        <h3>{{__('property information')}}</h3>
        <p><span> {{__('hotel name')}} : </span> <strong>{{$property->property_name}}</strong></p>
        <p><span> {{__("year")}} : </span> <strong>{{date("Y")}}</strong></p>
        <p><span> Score : </span> <strong>{{$score}}%</strong></p>
        <p><span> {{__('last update')}} : </span> <strong>{{$property->updated_at}}</strong></p>
        <!-- <p><span> Username : </span> <strong>Pritom</strong></p> -->
    </div>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>{{__('question')}}</th>
                <th>{{__('answer')}}</th>
                <th>{{__('remarks')}}</th>
                <th>{{__('deadline')}}</th>
                <th>{{__('reminder')}}</th>
                <th>{{__('evidence')}}</th>
            </tr>
        </thead>
        <tbody>
        	@if(!empty($groupQuestions))
     		@php 
     		$count = 1;
            $alphabet = 0;
     		$serial = range('A', 'Z');
     		@endphp
        	@foreach ( $groupQuestions as $key => $questions )
        	
        	<tr>
        		<td colspan="7" style="background: #e9f0fa;">{{$serial[$alphabet]}}. {{ $key }}</td>
        	</tr>
        	
        	@if(!empty( $questions ))
        	@foreach ($questions as $singleQuestion)
            @php 
            $answers = abc($singleQuestion->id, $property->id, $term);
            
            @endphp
            
            <tr class="property-table" style="cursor:pointer;" data-href="/{{app()->getLocale()}}/property/{{$property->id}}/question/?question={{$singleQuestion->id}}">
                
                <td><strong>{{$count}}</strong></td>
                <td><a href="{{route('question', ['language' => app()->getLocale(),'id'=>$property->id, 'question'=>$singleQuestion->id])}}">{{$singleQuestion->ques_name}}</a></td>
                <td> 
                    @php
                    if(!empty($answers) && isset($answers->answer)){
                        if($answers->answer == "Yes"){
                            echo __('yes');
                        }

                        if($answers->answer == "No"){
                            echo __('no');
                        }

                        if($answers->answer == "N/A"){
                            echo __('n/a');
                        } 
                    }
                    @endphp
                </td>
                <td>
                    @php
                    if(!empty($answers) && isset($answers->remarks)){
                        echo $answers->remarks;
                    }
                    @endphp
                </td>
                <td>
                    @php
                    if(!empty($answers) && isset($answers->deadline)){
                        echo $answers->deadline;
                    }
                    @endphp
                </td>
                <td>
                    @php
                    if(!empty($answers) && isset($answers->reminder)){
                        echo $answers->reminder;
                    }
                    @endphp
                </td>
                <td>
                    @if(!empty($answers) && $answers->images->count() > 0)
                    Yes
                    @else
                    No
                    @endif
                </td>
               
            </tr>
            
            @php $count++ @endphp

            @endforeach
            @endif
            @php $alphabet++ @endphp
            @endforeach
            @else
            <h1>There are no questions of this property</h1>
            @endif

        </tbody>
    </table>
</div>

<div class="tbl-mobile-accordion-wrapper" style="margin:20px; margin-bottom: 60px; display: none;">

    <div class="table mobile-accordion">
        <div class="thead">
            <div class="tr">
                <div class="th">{{__('question')}}</div>
                <div class="th">{{__('answer')}}</div>
                <div class="th">{{__('remarks')}}</div>
                <!-- <div class="th">Priority</div> -->
                <div class="th">{{__('deadline')}}</div>
                <div class="th">{{__('reminder')}}</div>
                <div class="th">{{__('evidence')}}</div>
                <th class="th">{{__('action')}}</th>
            </div>
        </div>
        <div class="tbody">

            @if(!empty($groupQuestions))
            @php 
            $count = 1;
            $alphabet = 0;
            $serial = range('A', 'Z');
            @endphp

            @foreach ( $groupQuestions as $key => $questions )
            
            @if(!empty( $questions ))
            @foreach ($questions as $singleQuestion)

            @php 
            $answers = abc($singleQuestion->id, $property->id, $term);
            
            @endphp

            <div class="rh"><span>{{$singleQuestion->ques_name}}</span></div>
            <div class="tr">
                <div class="td" data-header="{{__('question')}}:">{{$singleQuestion->ques_name}}</div>
                <div class="td" data-header="{{__('answer')}}:">
                     
                    @php
                    if(!empty($answers) && isset($answers->answer)){
                        if(!empty($answers) && isset($answers->answer)){
                            if($answers->answer == "Yes"){
                                echo __('yes');
                            }

                            if($answers->answer == "No"){
                                echo __('no');
                            }

                            if($answers->answer == "N/A"){
                                echo __('n/a');
                            } 
                        }
                    }
                    @endphp
               
                </div>
                <div class="td" data-header="{{__('remarks')}}:">
                    @php
                    if(!empty($answers) && isset($answers->remarks)){
                        echo $answers->remarks;
                    }
                    @endphp
                </div>
                <div class="td" data-header="{{__('deadline')}}:">
                    @php
                    if(!empty($answers) && isset($answers->deadline)){
                        echo $answers->deadline;
                    }
                    @endphp
                </div>
                <div class="td" data-header="{{__('reminder')}}:">
                    @php
                    if(!empty($answers) && isset($answers->reminder)){
                        echo " &nbsp ". $answers->reminder;
                    }
                    @endphp
                </div>
                <div class="td" data-header="{{__('evidence')}}:">
                    @if(!empty($answers) && $answers->images->count() > 0)
                    Yes
                    @else
                    No
                    @endif
                </div>
                <div style="margin:20px; text-align: center;">
                    <a href="{{route('question', ['language' => app()->getLocale(),'id'=>$property->id, 'question'=>$singleQuestion->id])}}"><button style="background: #67d5bf; color: #fff;border:none;">{{__('details')}}</button></a>
                </div>
            </div>

            @php $count++; @endphp
            @endforeach
            @endif
            @php $alphabet++ @endphp
            @endforeach
            @else
            <h1>There are no questions of this property</h1>
            @endif

        </div>
    </div>
</div>

@endsection
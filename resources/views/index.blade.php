@extends('layouts.app')

@section('language')

<div class="language-switcher-wrapper">
    <a href="{{route(Route::currentRouteName(), 'en')}}"><img src="{{asset('assets/images/en.png')}}" class="fr"></a>
    <a href="{{route(Route::currentRouteName(), 'nl')}}"><img src="{{asset('assets/images/nl.png')}}" class="en"></a>
</div>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
</div>

<section class="section section-default mt-none mb-none section-services">
    <div class="container">
        <h2 class="mb-sm"><strong>{{__('select terms')}}</strong></h2>
        <div class="row">
            @if(!empty($terms))
            @foreach($terms as $term)
            <div class="col-xs-12 col-sm-4 col-lg-2">
                <a href="{{route('property.term',['language' => app()->getLocale(),'term'=>$term->id] )}}">
                
                    <div class="service-block-container">
                        <div class="service-block">
                            <div class="service-underlay">
                                <span class="service-name">{{$term->term_name}}</span>
                            </div>
                            <!-- <span class="service-icon">
                                <em class="fa fa-code"></em>
                            </span> -->
                            
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>


@endsection

@extends('layouts.app')

@section('language')

<div class="language-switcher-wrapper">

    <a href="{{route(Route::currentRouteName(), ['language' => 'en', 'id' => $property->id, 'question' => $value])}}"><img src="{{asset('assets/images/en.png')}}" class="fr"></a>
    <a href="{{route(Route::currentRouteName(), ['language' => 'nl', 'id' => $property->id, 'question' => $value])}}"><img src="{{asset('assets/images/nl.png')}}" class="en"></a>

</div>

@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">

            <div class="back-btn-wrapper">
                <a href="{{route('property', ['language' => app()->getLocale(),'id'=>$property->id])}}">
                    <button class="button" style="background: #67d5bf; color: #fff; border:none;">Go Back</button>
                </a>
            </div>

            <form id="msform" action="{{route('answers.submit',['language'=> app()->getLocale(), 'property' => $property->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                <ul id="progressbar">
                    @php $count = 1 @endphp
                    @foreach($questions as $question)
                        @php
                        if($question->id == $value){
                            $abc = "class=active";                              
                        } else {
                            $abc = '';
                        }
                        @endphp

                        <li {{$abc}}></li>
                        
                        @php $count++ @endphp

                    @endforeach

                </ul>

                @if (session('status'))
                    <div class="alert alert-success" role="alert" style="text-align:center;">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="question-wrapper" property_id="{{$property->id}}">

                    @php 
                    $i = 1;
                    $propertyId = $property->id;
                    $total = count($questions)
                    @endphp
                    
                    @foreach($questions as $key => $question)
                        @php
                        if( $key != count( $questions ) - 1 ){
                            $last = false;
                        } else {
                            $last = true;
                        }

                        @endphp
                        <x-questioncomponent :index="$i" :question="$question" :last="$last" :total="$total"  :propertyId="$propertyId" />
                        @php $i++ @endphp
                    @endforeach

                </div>

            </form>

        </div>
    </div>
</div>

@endsection






@component('mail::message')

@component('mail::panel')

Content : This is a follow up reminder of a question.<br/><br/>
Hotel Name : {{$reminders['property']['property_name']}} <br/>
Term : {{$reminders['term']['term_name']}} <br/>
Question Number & Question : {{$reminders['question']['id']}}. {{$reminders['question']['ques_name']}} <br/>
Answer : {{$reminders['answer']}} <br/>
Deadline : {{$reminders['deadline']}} <br/>
Remarks : {{$reminders['remarks']}} <br/>
Followup remind send to : {{ $usernames }} <br/>

@endcomponent

Thanks,<br>
FHS
@endcomponent

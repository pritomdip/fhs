@extends('layouts.app')

@section('language')

<div class="language-switcher-wrapper">
    <a href="{{route(Route::currentRouteName(), ['language' => 'en', 'term' => $term])}}"><img src="{{asset('assets/images/en.png')}}" class="fr"></a>
    <a href="{{route(Route::currentRouteName(), ['language' => 'nl', 'term' => $term])}}"><img src="{{asset('assets/images/nl.png')}}" class="en"></a>
</div>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
</div>

<div class="bs-example" data-example-id="striped-table" style="padding:20px;">

    <div class="property-info-wrapper" style="padding-bottom:30px;">
        <h3>{{__('properties')}}</h3>
        
    </div>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th style="width:15%; text-align: center;">Logo</th>
                <th style="width:50%%; text-align: center;">{{__('name')}}</th>
                <th style="width:5%; text-align: center;">{{__('score')}}</th>
                <th style="text-align: center;">{{__('action')}}</th>
            </tr>
        </thead>
        <tbody>

        	@foreach($properties as $property)
            
            <tr class="property-table" style="cursor:pointer;" data-href="/{{app()->getLocale()}}/property/{{$property->id}}">
                
                <td class="property-tbl-td"><a href="{{route('property', ['language' => app()->getLocale(),'id'=>$property->id])}}">
                	@php
                	if(!empty($property->getPropertyDesc->logo)){
                        $image = asset('admin_assets/assets/image/' . $property->getPropertyDesc->logo);
                	} else {
                		$image = '';
                	}
                	@endphp
                	@if(!empty($image))
                	<img src="{{$image}}" style="height:100px;" />
                	@endif
                </a></td>
                <td class="property-tbl-td"> 
                    {{$property->property_name}}
                </td>
                <td class="property-tbl-td">
                    {{$property->getScore($term)}}%
                </td>
                <td class="property-tbl-td">
                    <button style="background: #67d5bf; color: #fff;border:none;">{{__('details')}}</button>
                </td>
               
            </tr>

            @endforeach

        </tbody>
    </table>
</div>


<div class="tbl-mobile-accordion-wrapper" style="margin:20px; display: none;">

    <div class="table mobile-accordion">
        <div class="thead">
            <div class="tr">

                <th class="th">{{__('logo')}}</th>
                <th class="th">{{__('name')}}</th>
                <th class="th">{{__('score')}}</th>
                <th class="th">{{__('action')}}</th>

            </div>
        </div>
        <div class="tbody">

            @foreach ($properties as $property)

            <div class="rh"><span>{{$property->property_name}}</span></div>
            <div class="tr">
                <div class="td" data-header="{{__('logo')}}:">
                	@php
                	if(!empty($property->getPropertyDesc->logo)){
                        $image = asset('admin_assets/assets/image/' . $property->getPropertyDesc->logo);
                	} else {
                		$image = '';
                	}
                	@endphp
                	@if(!empty($image))
                	<img src="{{$image}}" style="height:100px;" />
                	@endif
                </div>
                <div class="td" data-header="{{__('name')}}:">{{$property->property_name}}</div>
                <div class="td" data-header="{{__('score')}}:">{{$property->getScore($term)}}%</div>
                <div style="margin:20px; text-align: center;">
                	<a href="{{route('property', ['language' => app()->getLocale(),'id'=>$property->id])}}">
	                	<button style="background: #67d5bf; color: #fff;border:none;">{{__('details')}}</button>
	                </a>
                </div>
            </div>

            @endforeach

        </div>
    </div>
</div>


@endsection

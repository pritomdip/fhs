<fieldset style="display: none;">

	<div class="content-wrapper">
        <p>{{__('question')}} {{$index}} {{__('of')}} {{$total}}</p>

        <div class="question-wrapper">
            <p class="bold mb-0">Q. {{$question->ques_name}}</p>
            <p>{{$question->ques_desc}}</p>
        </div>

		<input type="hidden" name="question_id[]" class="question_id" value="{{$question->id}}">

	    <div class="input-wrapper">
            <input type="radio" required="true" id="test{{$index*2-1}}" class="checkbox-group" name="answer[{{$index}}]" value="Yes" 
            @php
		        if(!empty($answers) && isset($answers->answer) && $answers->answer == 'Yes' ){
		        	echo "checked";
		    	} 
            @endphp />
            <label for="test{{$index*2-1}}" class="mr-20">{{__('yes')}}</label>

            <input type="radio" id="test{{$index*20-2}}" class="checkbox-group" name="answer[{{$index}}]" value="No" 
            @php
		        if(!empty($answers) && isset($answers->answer) && $answers->answer == 'No' ){
		        	echo "checked";
		    	} 
            @endphp />
            <label for="test{{$index*20-2}}" class="mr-20">{{__('no')}}</label>

            <input type="radio" id="test{{$index*80-3}}" class="checkbox-group" name="answer[{{$index}}]" value="N/A" @php
		        if(!empty($answers) && isset($answers->answer) && $answers->answer == 'N/A' ){
		        	echo "checked";
		    	} 
            @endphp />
            <label for="test{{$index*80-3}}" class="mr-20">{{__('n/a')}}</label>

	    </div>

	    <div class="remarks-wrapper">
	        <label for="remarks">{{__('remarks')}} : </label> 
	        <textarea name="remarks[]" class="remarks">@php
		        if(!empty($answers) && isset($answers->remarks)){
		        	echo $answers->remarks;
		    	} 
	    	@endphp</textarea>
	    </div>

	    <div class="deadline-wrapper">
	    	<label for="priority" class="priority-lebel">{{__('priority')}} : </label>
	    	<select name="priority[]" class="form-control priority-select">
	    		<option value="">{{__('select')}}</option>
	    		<option value="low" @php if(!empty($answers) && isset($answers->priority) && $answers->priority == 'low' ){
		        	echo "selected";
		    	} @endphp >{{__('low')}}</option>
	    		<option value="medium" @php if(!empty($answers) && isset($answers->priority) && $answers->priority == 'medium' ){
		        	echo "selected";
		    	} @endphp >{{__('medium')}}</option>
	    		<option value="high" @php if(!empty($answers) && isset($answers->priority) && $answers->priority == 'high' ){
		        	echo "selected";
		    	} @endphp >{{__('high')}}</option>
	    	</select>
	    </div>

	    <div class="deadline-wrapper">
	        <label for="deadline">{{__('set deadline')}} : </label> 
	        <input type="text"  class="datepicker deadline" name="deadline[]" value="@php
	        	if(!empty($answers) && isset($answers->deadline)){
		        	echo $answers->deadline;
		    	}
	        @endphp" />
	    </div>  

	    <div class="deadline-wrapper">
	        <label for="reminder">{{__('Follow Up Reminder')}} : </label> 
	        <input type="text" class="datepicker reminder" name="reminder[]" value="@php
	        	if(!empty($answers) && isset($answers->reminder)){
		        	echo $answers->reminder;
		    	}
	         @endphp" />
	    </div>  

	    <div class="deadline-wrapper">

	        <div class="select2-wrapper">
	        	<label for="user_id">{{__("Send Follow Up Notifications to User?")}}</label>

	        	<select name="users[{{$index}}][][user_id]" class="select-multiple" multiple style="width:45%;">

	        		@foreach ($users as $user)
	        			@php 
	        			$a = '';

		        		if(!empty($answers) && $answers->users->count() > 0){
		        			foreach ($answers->users as $value) {
		        				if($value->id == $user->id) $a = 'selected';
		        			}
		        		}
		        		@endphp

		        		<option value="{{$user->id}}" <?php echo $a; ?> >@php echo ucfirst($user->last_name) @endphp</option>
		        		
		        	@endforeach
	        	</select>
	        </div>
	    </div> 

	    <div class="deadline-wrapper">
			<label for="file-input" class="button hollow">{{__('Attach Evidence')}}</label>
		    <input type="file" name="image[{{$index}}][][image]" id="file-input" class="file-input image" multiple />

		    @if(!empty($answers) && $answers->images->count() > 0)

		    <div class="gallery-wrapper">
		    	<label>{{__("Previously Uploaded")}}</label>
		    	@foreach($answers->images as $image_url)
		    	<div class="gallery-items">
			    	<!-- <img src="{{ asset('/storage/images/' . $image_url->image)}}" style="width:100px; height:auto;" attribute="{{$image_url->image}}"/> -->

			    	<iframe class="evidence-imgs" src="{{ asset('/storage/images/' . $image_url->image)}}" style="width:150px; height:auto; overflow:hidden;top:0px; left:0px; " allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" frameborder="0" scrolling="no" attribute="{{$image_url->image}}"></iframe>

			    	<span class="cross image-delete" lang="{{app()->getLocale()}}"></span>
		    	</div>
		    	@endforeach
		    </div>

		    @endif

		    <!-- <div class="gallery"></div> -->

			<!-- <div id="thumb-output"></div> -->
		</div>

	</div>

    @if($index != 1)
    	<input type="button" name="previous" class="previous action-button" value="{{__('Previous')}}" style="width:30%; text-align: center;" />
    @endif

    	<input type="submit" name="submit" class="submit action-button" value="{{__('save')}}" style="width:30%; text-align: center;" />

    @if($last)
    	<input type="submit" name="submit" class="submit action-button" value="{{__('submit')}}" style="width:30%; text-align: center;" />
    @else
    	<input type="button" name="next" class="next action-button" value="{{__('next')}}" style="width:30%; text-align: center;" />
    @endif

</fieldset>
@extends('admin.partials.layout')
@section('title', 'Edit Profile')
@extends('admin.partials.header')
@extends('admin.partials.sidebar')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Edit Profile</h3>
                    
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(Session::has('error_msg'))
                        <p class="alert alert-danger">{{ Session::get('error_msg') }}</p>
                    @endif

					@if(Session::has('success'))
                        <p class="alert alert-info">{{ Session::get('success') }}</p>
                    @endif

                	<form action="{{route('admin.change.password', $admin->id)}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Name</label>
						    	<input type="text" name="name" class="form-control" required placeholder="Enter Name" id="username" value="{{$admin->name}}" disabled>
						  	</div>
						</div>

						
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Email address</label>
						    	<input type="email" name="email" class="form-control" aria-describedby="emailHelp" id="email" required placeholder="Enter email" value="{{$admin->email}}" disabled >
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="password">New Password</label>
						    	<input type="password" name="password" class="form-control" id="password" placeholder="Enter email" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="password">Confirm Password</label>
						    	<input type="password" name="confirm_password" class="form-control" id="password" placeholder="Enter email" required>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>         
               
            </div>
        </div>
    </div>

    



@endsection
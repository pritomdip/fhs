@extends('admin.partials.layout')
@section('title', 'Edit Property')
@extends('admin.partials.header')
@section('property', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update Property</h3>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<form action="{{route('admin.property.update', $property->id)}}" method="post" enctype="multipart/form-data">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="propertyname">Proeprty Name<span class="bg-red">*</span></label>
						    	<input type="text" name="property_name" class="form-control" placeholder="Enter Property Name" id="propertyname" value="{{$property->property_name}}" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="propertyslug">Property Slug</label>
						    	<input type="text" name="property_slug" class="form-control" placeholder="Enter Property Slug" id="propertyslug" value="{{$property->property_slug}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="companyname">Company Name</label>
						    	<input type="text" name="company_name" class="form-control"  placeholder="Enter Address" id="companyname" value="{{$desc->company_name}}">
						  	</div>
						</div>              		

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Property Logo</label>
						    	<input type="file" name="logo" class="form-control" id="logo" value="{{$desc->logo}}">		    	
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="address">Address One</label>
						    	<input type="text" name="address" class="form-control"  placeholder="Enter Address" id="address" value="{{$desc->address}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="address">Address Two</label>
						    	<input type="text" name="address_two" class="form-control"  placeholder="Enter Address" id="address" value="{{$desc->address_two}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="city">City</label>
						    	<input type="text" name="city" class="form-control"  placeholder="Enter city" id="city" value="{{$desc->city}}" />
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="postcode">Postcode</label>
						    	<input type="text" name="postcode" class="form-control"  placeholder="Enter Postcode" id="postcode" value="{{$desc->postcode}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="country">Country</label>
						    	<input type="text" name="country" class="form-control"  placeholder="Enter Country" id="country" value="{{$desc->country}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Tel</label>
						    	<input type="text" name="tel" class="form-control" placeholder="Enter Telephone Number" id="email" value="{{$desc->tel}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="manager">Manager Name<span class="bg-red">*</span></label>
						    	<input type="text" name="manager" class="form-control"  placeholder="Enter Manager Name" id="manager" value="{{$desc->manager}}" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Property Email Address<span class="bg-red">*</span></label>
						    	<input type="email" name="email" class="form-control" placeholder="Enter email" id="email" value="{{$desc->email}}" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="propertstatus">status</label>
						    	<select name="property_status" class="form-control" id="propertstatus">
						    		<option value="a" <?php if($property->property_status =="a") echo "selected"; ?> >Active</option>
						    		<option value="b" <?php if($property->property_status =="b") echo "selected"; ?>>Inactive</option>
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>        
               
            </div>
        </div>
    </div>

@endsection
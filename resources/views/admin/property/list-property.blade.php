@extends('admin.partials.layout')
@section('title', 'List property')
@extends('admin.partials.header')
@section('property', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Properties List</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.property.add')}}" class="btn btn-primary heading-wrapper-button">Add New</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<div class="tbl-header">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <thead>
				                <tr>
				                    <th>SL</th>
				                    <th>Property Name</th>
				                    <th>Manager</th>
				                    <th>Status</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				        </table>
				    </div>
				    <div class="tbl-content">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <tbody>
				            	@php $count = 0 @endphp
				            	@foreach ($properties as $property)

				            		<tr>
					                    <td>{{ $count + $properties->firstItem() }}</td>
					                    <td>{{$property->property_name}}</td>
					                   
					                    <td>
					                    	@if(!empty($property->getPropertyDesc->manager))
												{{$property->getPropertyDesc->manager}}
					                    	@endif
					                    </td>
					                    
					                    <td> <?php echo ( $property->property_status == "a" ) ? 'active' : 'inactive'; ?></td>

					                    <td>
					                    	<a href="{{route('admin.property.edit', $property->id)}}"><button>Edit</button></a>
					                    	<a href="{{route('admin.property.delete', $property->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><button>Delete</button></a>
					                    </td>
					                </tr>

						        <?php $count++; ?>
						    	@endforeach

				            </tbody>
				        </table>
				    </div>

                </div>
              
              	<div class="click-option" style="margin:20px;">
                	{{$properties->links()}}
                </div>  
               
            </div>
        </div>
    </div>

    



@endsection
@extends('admin.partials.layout')
@section('title', 'Add Property')
@section('property', 'active')
@extends('admin.partials.header')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Add Property</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.property.list')}}" class="btn btn-primary heading-wrapper-button">View All</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

                	<form action="{{route('admin.property.submit')}}" method="post" enctype="multipart/form-data">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Property Name<span class="bg-red">*</span></label>
						    	<input type="text" name="property_name" class="form-control" placeholder="Enter Property Name" id="username" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Property Slug</label>
						    	<input type="text" name="property_slug" class="form-control" placeholder="Enter Property Slug" id="slug">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="companyname">Company Name</label>
						    	<input type="text" name="company_name" class="form-control"  placeholder="Enter Address" id="companyname">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Property Logo</label>
						    	<input type="file" name="logo" class="form-control" id="logo">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="address">Address One</label>
						    	<input type="text" name="address" class="form-control"  placeholder="Enter Address" id="address">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="address_two">Address Two</label>
						    	<input type="text" name="address_two" class="form-control"  placeholder="Enter Address" id="address_two">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="city">City</label>
						    	<input type="text" name="city" class="form-control"  placeholder="Enter city" id="city">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="postcode">Postcode</label>
						    	<input type="text" name="postcode" class="form-control"  placeholder="Enter Postcode" id="postcode">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="country">Country</label>
						    	<input type="text" name="country" class="form-control"  placeholder="Enter Country" id="country">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Tel</label>
						    	<input type="text" name="tel" class="form-control" placeholder="Enter Telephone Number" id="email">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="manager">Manager Name<span class="bg-red">*</span></label>
						    	<input type="text" name="manager" class="form-control"  placeholder="Enter Manager Name" id="manager" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Property Email Address<span class="bg-red">*</span></label>
						    	<input type="email" name="email" class="form-control" placeholder="Enter email" id="email" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Property status</label>
						    	<select name="property_status" class="form-control" id="status">
						    		<option value="a">Active</option>
						    		<option value="b">Inactive</option>
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>         
               
            </div>
        </div>
    </div>

    



@endsection
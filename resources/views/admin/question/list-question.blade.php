@extends('admin.partials.layout')
@section('title', 'List Question')
@extends('admin.partials.header')
@section('question', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Question List</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.question.add')}}" class="btn btn-primary heading-wrapper-button">Add New</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<div class="tbl-header">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <thead>
				                <tr>
				                    <th>SL</th>
				                    <th>Question Name</th>
				                    <th>Property</th>
				                    <th>Category</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				        </table>
				    </div>
				    <div class="tbl-content">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <tbody>
				            	@php $count = 0 @endphp
				            	@foreach ($questions as $question)
				            		<tr>
					                    <td>{{ $count + $questions->firstItem() }}</td>
					                    <td>{{$question->ques_name}}</td>
					                    <td>
					                    	<?php 
					                    	$properties_name = [];
					                    	foreach( $question->properties as $property ){
					                    		array_push($properties_name, $property->property_name);
					                    	}
					                    	
					                    	echo implode(', ', $properties_name);
					                    	?>

					                    </td>
					                    <td>{{$question->getCategory->cat_name}}</td>

					                    <td>
					                    	<a href="{{route('admin.question.edit', $question->id)}}"><button>Edit</button></a>
					                    	<a href="{{route('admin.question.delete', $question->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><button>Delete</button></a>
					                    </td>
					                </tr>

						        <?php $count++; ?>
						    	@endforeach

				            </tbody>
				        </table>
				    </div>

                </div>
              
              	<div class="click-option" style="margin:20px;">
                	{{$questions->links()}}
                </div>  
               
            </div>
        </div>
    </div>

    



@endsection
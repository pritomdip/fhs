@extends('admin.partials.layout')
@section('title', 'Edit Question')
@extends('admin.partials.header')
@section('question', 'active')
@extends('admin.partials.sidebar')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update Question</h3>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<form action="{{route('admin.question.update', $question->id)}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="propertyname">Question Name<span class="bg-red">*</span></label>
						    	<input type="text" name="ques_name" class="form-control" required placeholder="Enter Question Name" id="quesname" value="{{$question->ques_name}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Question Description</label>
						    	<textarea name="ques_desc" class="form-control">{{$question->ques_desc}}</textarea>
						    	
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Category<span class="bg-red">*</span></label>
						    	<select name="category_id" required class="form-control" id="category_id">
						    		<option value="">Please Select</option>
						    		@foreach($categories as $category)
						    		<option value="{{$category->id}}" <?php if($question->getCategory->cat_name == $category->cat_name) echo "selected"; ?> >{{$category->cat_name}}</option>
						    		@endforeach
						    	</select>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Property<span class="bg-red">*</span></label>
						    	<select name="property_id[]" class="form-control select-multiple" id="property_id" required multiple="multiple">
						    		@foreach($properties as $property)
						    		<option value="{{$property->id}}" 
						    			<?php 
						    			foreach ($question->properties as $selected_property) {
						    				if($selected_property->property_name == $property->property_name) echo "selected";
						    			}
						    			?> >
						    			{{$property->property_name}}</option>
						    		@endforeach
						    	</select>
						  	</div>
						</div>

						<div class="clearfix"></div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Term<span class="bg-red">*</span></label>
						    	<select name="term_id[]" required class="form-control select-multiple" id="property_id" multiple="multiple">
						    		@foreach($terms as $term)
						    		<option value="{{$term->id}}" 
						    			<?php 
						    			foreach ($question->terms as $selected_term) {
						    				if($selected_term->term_name == $term->term_name) echo "selected";
						    			}
						    			?> >
						    			{{$term->term_name}}</option>
						    		@endforeach
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>        
               
            </div>
        </div>
    </div>

@endsection
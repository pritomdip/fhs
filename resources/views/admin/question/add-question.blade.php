@extends('admin.partials.layout')
@section('title', 'Add Question')
@section('question', 'active')
@extends('admin.partials.header')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Add Queation</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.question.list')}}" class="btn btn-primary heading-wrapper-button">View All</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

                	<form action="{{route('admin.question.submit')}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Question Name<span class="bg-red">*</span></label>
						    	<input type="text" name="ques_name" class="form-control" reuired placeholder="Enter Question Name" id="username" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Question Description</label>
						    	<textarea name="ques_desc" class="form-control"></textarea>
						    	
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Category<span class="bg-red">*</span></label>
						    	<select name="category_id" class="form-control" id="category_id" required>
						    		<option value="">Please Select</option>
						    		@foreach($categories as $category)
						    		<option value="{{$category->id}}">{{$category->cat_name}}</option>
						    		@endforeach
						    	</select>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Property<span class="bg-red">*</span></label>
						    	<select name="property_id[]" class="form-control select-multiple" id="property_id" required multiple="multiple">
						    		@foreach($properties as $property)
						    		<option value="{{$property->id}}">{{$property->property_name}}</option>
						    		@endforeach
						    	</select>
						  	</div>
						</div>
						<div class="clearfix"></div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Terms<span class="bg-red">*</span></label>
						    	<select name="term_id[]" class="form-control select-multiple" id="term_id" required multiple="multiple">
						    		@foreach($terms as $term)
						    		<option value="{{$term->id}}">{{$term->term_name}}</option>
						    		@endforeach
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>         
               
            </div>
        </div>
    </div>

    



@endsection
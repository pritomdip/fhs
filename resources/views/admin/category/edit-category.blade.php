@extends('admin.partials.layout')
@section('title', 'Edit Category')
@extends('admin.partials.header')
@section('category', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update Category</h3>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<form action="{{route('admin.category.update', $category->id)}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="propertyname">Category Name<span class="bg-red">*</span></label>
						    	<input type="text" name="cat_name" class="form-control" required placeholder="Enter Category Name" id="propertyname" value="{{$category->cat_name}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="catstatus">status</label>
						    	<select name="cat_status" class="form-control" id="catstatus">
						    		<option value="a" <?php if($category->cat_status =="a") echo "selected"; ?> >Active</option>
						    		<option value="b" <?php if($category->cat_status =="b") echo "selected"; ?>>Inactive</option>
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>        
               
            </div>
        </div>
    </div>

@endsection
@extends('admin.partials.layout')
@section('title', 'Add Category')
@section('Category', 'active')
@extends('admin.partials.header')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Add Category</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.category.list')}}" class="btn btn-primary heading-wrapper-button">View All</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

                	<form action="{{route('admin.category.submit')}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Category Name<span class="bg-red">*</span></label>
						    	<input type="text" name="cat_name" class="form-control" placeholder="Enter Category Name" id="cat_name" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">Category status</label>
						    	<select name="cat_status" class="form-control" id="status">
						    		<option value="a">Active</option>
						    		<option value="b">Inactive</option>
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>         
               
            </div>
        </div>
    </div>

    



@endsection
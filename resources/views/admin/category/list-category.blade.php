@extends('admin.partials.layout')
@section('title', 'List Category')
@extends('admin.partials.header')
@section('category', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Categories List</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.category.add')}}" class="btn btn-primary heading-wrapper-button">Add New</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<div class="tbl-header">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <thead>
				                <tr>
				                    <th>SL</th>
				                    <th>Category Name</th>
				                    <th>Status</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				        </table>
				    </div>
				    <div class="tbl-content">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <tbody>
				            	@php $count = 0 @endphp
				            	@foreach ($categories as $category)

				            		<tr>
					                    <td>{{ $count + $categories->firstItem() }}</td>
					                    <td>{{$category->cat_name}}</td>

					                    <td> <?php echo ( $category->cat_status == "a" ) ? 'active' : 'inactive'; ?></td>

					                    <td>
					                    	<a href="{{route('admin.category.edit', $category->id)}}"><button>Edit</button></a>
					                    	<a href="{{route('admin.category.delete', $category->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><button>Delete</button></a>
					                    </td>
					                </tr>

						        <?php $count++; ?>
						    	@endforeach

				            </tbody>
				        </table>
				    </div>

                </div>
              
              	<div class="click-option" style="margin:20px;">
                	{{$categories->links()}}
                </div>  
               
            </div>
        </div>
    </div>

    



@endsection
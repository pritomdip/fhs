<aside>
    <nav class="rad-sidebar">
        <ul class="sidebar-wrapper">
            <li class="@yield('dashboard')">
                <a href="#" class="menu-item">
                    <i class="fa fa-dashboard"><span class="icon-bg rad-bg-success"></span></i>
                    <span class="rad-sidebar-item">Dashboard</span>
                </a>
            </li>

            <li class="sub-menu @yield('users')">
                <a href="#">
                    <i class="fa fa-wrench">
                        <span class="icon-bg rad-bg-danger"></span>
                    </i>
                    <span class="rad-sidebar-item">Users <div class='fa fa-caret-down right pull-right'></div> </span>
                </a>

                <ul>
                    <li>
                        <a href="{{Route('admin.users.add')}}">
                            <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                            <span class="rad-sidebar-item">Add New</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{Route('admin.users.list')}}">
                            <i class="fa fa-bar-chart-o">
                                <span class="icon-bg rad-bg-danger"></span>
                            </i>
                            <span class="rad-sidebar-item">View All</span>
                        </a>
                    </li>
                </ul>

            </li>


            <li class="sub-menu @yield('property')">
                <a href="#">
                    <i class="fa fa-wrench">
                        <span class="icon-bg rad-bg-danger"></span>
                    </i>
                    <span class="rad-sidebar-item">Properties <div class='fa fa-caret-down right pull-right'></div> </span>
                </a>

                <ul>
                    <li>
                        <a href="{{Route('admin.property.add')}}">
                            <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                            <span class="rad-sidebar-item">Add Property</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{Route('admin.property.list')}}">
                            <i class="fa fa-bar-chart-o">
                                <span class="icon-bg rad-bg-danger"></span>
                            </i>
                            <span class="rad-sidebar-item">View All</span>
                        </a>
                    </li>
                </ul>

            </li>

            <li class="sub-menu @yield('category')">
                <a href="#">
                    <i class="fa fa-wrench">
                        <span class="icon-bg rad-bg-danger"></span>
                    </i>
                    <span class="rad-sidebar-item">Categories <div class='fa fa-caret-down right pull-right'></div> </span>
                </a>

                <ul>
                    <li>
                        <a href="{{Route('admin.category.add')}}">
                            <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                            <span class="rad-sidebar-item">Add Category</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{Route('admin.category.list')}}">
                            <i class="fa fa-bar-chart-o">
                                <span class="icon-bg rad-bg-danger"></span>
                            </i>
                            <span class="rad-sidebar-item">View All</span>
                        </a>
                    </li>
                </ul>

            </li>

            <li class="sub-menu @yield('question')">
                <a href="#">
                    <i class="fa fa-wrench">
                        <span class="icon-bg rad-bg-danger"></span>
                    </i>
                    <span class="rad-sidebar-item">Questions <div class='fa fa-caret-down right pull-right'></div> </span>
                </a>

                <ul>
                    <li>
                        <a href="{{Route('admin.question.add')}}">
                            <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                            <span class="rad-sidebar-item">Add Question</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{Route('admin.question.list')}}">
                            <i class="fa fa-bar-chart-o">
                                <span class="icon-bg rad-bg-danger"></span>
                            </i>
                            <span class="rad-sidebar-item">View All</span>
                        </a>
                    </li>
                </ul>

            </li>

            <li class="sub-menu @yield('question')">
                <a href="#">
                    <i class="fa fa-wrench">
                        <span class="icon-bg rad-bg-danger"></span>
                    </i>
                    <span class="rad-sidebar-item">Terms <div class='fa fa-caret-down right pull-right'></div> </span>
                </a>

                <ul>
                    <li>
                        <a href="{{Route('term.create')}}">
                            <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                            <span class="rad-sidebar-item">Add Term</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{Route('term.index')}}">
                            <i class="fa fa-bar-chart-o">
                                <span class="icon-bg rad-bg-danger"></span>
                            </i>
                            <span class="rad-sidebar-item">View All</span>
                        </a>
                    </li>
                </ul>

            </li>

            <!-- <li class='sub-menu'>
                <a href="#">
                    <i class="fa fa-wrench">
                        <span class="icon-bg rad-bg-danger"></span>
                    </i>
                    <span class="rad-sidebar-item">Settings <div class='fa fa-caret-down right pull-right'></div> </span>
                </a>

                <ul>
                    <li>
                        <a href="#">
                            <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                            <span class="rad-sidebar-item">Settings</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="#">
                            <i class="fa fa-bar-chart-o">
                                <span class="icon-bg rad-bg-danger"></span>
                            </i>
                            <span class="rad-sidebar-item">Ticket status</span>
                        </a>
                    </li>
                </ul>

            </li>

            <li><a href="#" class="snooz"><i class="fa fa-line-chart"><span class="icon-bg rad-bg-primary"></span></i><span class="rad-sidebar-item">Call trends</span></a></li>
            <li><a href="#" class="done"><i class="fa fa-area-chart"><span class="icon-bg rad-bg-warning"></span></i><span class="rad-sidebar-item">Heat maps</span></a></li>
            <li>
                <a href="#">
                    <i class="fa fa-wrench"><span class="icon-bg rad-bg-violet"></span></i>
                    <span class="rad-sidebar-item">Settings</span>
                </a>
            </li> -->
        </ul>
    </nav>
</aside>
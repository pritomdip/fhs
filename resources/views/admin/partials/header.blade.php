<section>
    <header>
        <nav class="rad-navigation">
            <div class="rad-logo-container">
                <a href="#" class="rad-logo"><i class=" fa fa-recycle"></i> Fhs</a>
                <a href="#" class="rad-toggle-btn pull-right"><i class="fa fa-bars margin-t7"></i></a>
            </div>
            <a href="#" class="rad-logo-hidden">Fhs</a>
            <div class="rad-top-nav-container">
                <a href="" class="brand-icon"><i class="fa fa-recycle"></i></a>
                <ul class="pull-right links">
                    <li class="rad-dropdown no-color">
                        <a class="rad-menu-item" href="#"><img class="rad-list-img sm-img" src="{{asset('admin_assets/assets/image/user_img.jpg')}}" alt="me" /></a>

                        <ul class="rad-dropmenu-item sm-menu">
                            <li class="rad-notification-item">
                                <a class="rad-notification-content lg-text" href="{{route('admin.profile', Auth::id())}}"><i class="fa fa-edit"></i>Profile</a>
                            </li>
                            <li class="rad-notification-item">
                                <a class="rad-notification-content lg-text" href="{{route('admin.logout')}}"><i class="fa fa-power-off"></i> Sign out</a>
                            </li>
                        </ul>
                    </li>

                    <!-- Add here beside profile -->

                </ul>

            </div>
        </nav>
    </header>
</section>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <title>{{ config('app.name', 'FHS') }}</title> -->
    <title>@yield('title') - FHS</title> 

	<!-- Bootstrap core CSS -->
    <link href = {{ asset("libraries/bootstrap/css/bootstrap.min.css") }} rel="stylesheet" />
    <link href = {{ asset("libraries/font-awesome/css/font-awesome.min.css") }} rel="stylesheet" />
    <link href = {{ asset("admin_assets/assets/css/style.css") }} rel="stylesheet" />
    <link href = {{ asset("libraries/select2/select2.min.css") }} rel="stylesheet" />
</head>

<body>

    <div class="preload-wrapper" style="">
        <div class="lds-circle"><div></div></div>
    </div>

    @yield('header')

    @yield('sidebar')

    
    
    <main>
        <section>
            <div class="rad-body-wrapper">
                <div class="container-fluid">
                    @yield('content')

                    <div class="row" style="margin-left:-20px;">
                        <div class="com-xs-12">
                            <div id="footer" style="padding:20px; background: #000; color: #fff;">
                                <div> 
                                    copyright reserved
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
   

    <script src="{{ asset("admin_assets/assets/js/jquery.min.js") }}"></script>
    <script src="{{ asset("admin_assets/assets/js/jquery-ui.js") }}"></script>
    <script src="{{ asset("admin_assets/assets/js/themeplate.js") }}"></script>
    <script src="{{ asset("libraries/select2/select2.full.js") }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset("admin_assets/assets/js/scripts.js") }}"></script>

</body>







</html>
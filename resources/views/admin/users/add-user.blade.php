@extends('admin.partials.layout')
@section('title', 'Add user')
@section('users', 'active')
@extends('admin.partials.header')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Add User</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.users.list')}}" class="btn btn-primary heading-wrapper-button">View All</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

                	<form action="{{route('admin.user.submit')}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">First Name</label>
						    	<input type="text" name="first_name" class="form-control" required placeholder="Enter First Name" id="username">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Last Name</label>
						    	<input type="text" name="last_name" class="form-control" required placeholder="Enter Last Name" id="username">
						  	</div>
						</div>

                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Email address</label>
						    	<input type="email" name="email" class="form-control" aria-describedby="emailHelp" id="email" required placeholder="Enter email">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="password">Password</label>
						    	<input type="password" name="password" class="form-control" id="password" placeholder="Enter email" required>
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">status</label>
						    	<select name="status" class="form-control" id="status" required>
						    		<option value="a">Active</option>
						    		<option value="b">Inactive</option>
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>         
               
            </div>
        </div>
    </div>

    



@endsection
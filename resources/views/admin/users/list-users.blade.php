@extends('admin.partials.layout')
@section('title', 'List user')
@extends('admin.partials.header')
@section('users', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">User Lists</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('admin.users.add')}}" class="btn btn-primary heading-wrapper-button">Add New</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<div class="tbl-header">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <thead>
				                <tr>
				                    <th>SL</th>
				                    <th>First Name</th>
				                    <th>Last Name</th>
				                    <th>Email</th>
				                    <th>Status</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				        </table>
				    </div>
				    <div class="tbl-content">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <tbody>
				            	@php $count = 0 @endphp
				            	@foreach ($users as $user)

				            		<tr>
					                    <td>{{ $count + $users->firstItem() }}</td>
					                    <td>{{$user->first_name}}</td>
					                    <td>{{$user->last_name}}</td>
					                    <td>{{$user->email}}</td>
					                    <td> <?php echo ( $user->status == "a" ) ? 'active' : 'inactive'; ?></td>
					                    <td>
					                    	<a href="{{route('admin.users.edit', $user->id)}}"><button>Edit</button></a>
					                    	<a href="{{route('admin.users.delete', $user->id)}}"  onclick="return confirm('Are you sure you want to delete this item?');"><button>Delete</button></a>
					                    </td>
					                </tr>

						        <?php $count++; ?>
						    	@endforeach

				            </tbody>
				        </table>
				    </div>

                </div>
              
              	<div class="click-option" style="margin:20px;">
                	{{$users->links()}}
                </div>
                
               
            </div>
        </div>
    </div>

    



@endsection
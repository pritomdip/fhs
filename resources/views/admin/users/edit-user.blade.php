@extends('admin.partials.layout')
@section('title', 'Edit user')
@extends('admin.partials.header')
@section('users', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update User</h3>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<form action="{{route('admin.users.update', $user->id)}}" method="post">

                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">First Name</label>
						    	<input type="text" name="first_name" class="form-control" reuired placeholder="Enter Username" id="username" value="{{$user->first_name}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Last Name</label>
						    	<input type="text" name="last_name" class="form-control" reuired placeholder="Enter Username" id="username" value="{{$user->last_name}}">
						  	</div>
						</div>

                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Email address</label>
						    	<input type="email" name="email" class="form-control" aria-describedby="emailHelp" id="email" placeholder="Enter email" value="{{$user->email}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="email">Password</label>
						    	<input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" value="{{$user->password}}">
						  	</div>
						</div>

						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="status">status</label>
						    	<select name="status" class="form-control" id="status">
						    		<option value="a" <?php if($user->status =="a") echo "selected"; ?> >Active</option>
						    		<option value="b" <?php if($user->status =="b") echo "selected"; ?>>Inactive</option>
						    	</select>
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>
              
                
               
            </div>
        </div>
    </div>

@endsection
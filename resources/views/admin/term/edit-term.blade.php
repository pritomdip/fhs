@extends('admin.partials.layout')
@section('title', 'Edit Term')
@section('term', 'active')
@extends('admin.partials.header')
@extends('admin.partials.sidebar')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Edit Term</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('term.index')}}" class="btn btn-primary heading-wrapper-button">View All</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<form action="{{route('term.update', $term->id)}}" method="post">
                		@method('PUT')
                		@csrf
                		<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="Username">Term Name</label>
						    	<input type="text" name="term_name" class="form-control" placeholder="Enter Term Name" id="username" value="{{$term->term_name}}" required />
						  	</div>
						</div>

						<div class="col-md-12">
					  		<button type="submit" class="btn btn-primary pull-right">Submit</button>
					  	</div>

					</form>

                </div>         
               
            </div>
        </div>
    </div>

    



@endsection
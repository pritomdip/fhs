@extends('admin.partials.layout')
@section('title', 'List Term')
@extends('admin.partials.header')
@section('Term', 'active')
@extends('admin.partials.sidebar')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title list-heading-wrapper-left">Term List</h3>
                    <div class="list-heading-wrapper-right">
                    	<a href="{{route('term.create')}}" class="btn btn-primary heading-wrapper-button">Add New</a>
                    </div>
                </div>
                <div class="panel-body rad-map-container">

                	@if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                	<div class="tbl-header">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <thead>
				                <tr>
				                    <th>SL</th>
				                    <th>Term Name</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				        </table>
				    </div>
				    <div class="tbl-content">
				        <table cellpadding="0" cellspacing="0" border="0">
				            <tbody>
				            	@php $count = 0 @endphp
				            	@foreach ($terms as $term)

				            		<tr>
					                    <td>{{ $count + $terms->firstItem() }}</td>
					                    <td>{{$term->term_name}}</td>
					                    

					                    <td>
					                    	<a href="{{route('term.edit', $term->id)}}"><button>Edit</button></a>

					                    	<form method="post" action="{{route('term.destroy', $term->id)}}" style="display: inline-block;vertical-align: top; margin-left:10px;">
					                    		@method('DELETE')
					                    		@csrf
					                    		<button type="submit" onclick="return confirm('Are you sure you want to delete this item?');">Delete</button>
					                    	</form>
					                    </td>
					                </tr>

						        <?php $count++; ?>
						    	@endforeach

				            </tbody>
				        </table>
				    </div>

                </div>
              
              	<div class="click-option" style="margin:20px;">
                	{{$terms->links()}}
                </div>  
               
            </div>
        </div>
    </div>

    



@endsection
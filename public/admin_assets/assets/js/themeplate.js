 $(function() {

    function setFlatTheme() {
        $("body").toggleClass("flat-theme");
        $("#rad-color-opts").toggleClass("hide");
        $(".rad-chk-pin input[type=checkbox]").prop("checked", true);
    }

    setFlatTheme();

    $(window).on("scroll", function(e) {
        if ($(window).scrollTop() > 50) {
            $("body").addClass("sticky");
        } else {
            $("body").removeClass("sticky");
        }
    });


    $(".rad-toggle-btn").on("click", function() {
        $(".rad-logo-container").toggleClass("rad-nav-min");
        $(".rad-sidebar").toggleClass("rad-nav-min");
        $(".rad-body-wrapper").toggleClass("rad-nav-min");
        setTimeout(function() {
            initializeCharts();
        }, 200);
    });

    $("li.rad-dropdown > a.rad-menu-item").on("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(".rad-dropmenu-item").removeClass("active");
        $(this).next(".rad-dropmenu-item").toggleClass("active");
    });

    $(".rad-notification-item").on("click", function(e) {
        e.stopPropagation();
    });

    $(window).resize(function() {
        setTimeout(function() {
            initializeCharts();
        }, 200);
    });

    function initializeCharts() {
        $(".rad-chart").empty();
        $(".d3-*").empty();
    }

    function getTempl(img, text, position) {
        return (
            '<div class="rad-list-group-item ' +
            position +
            '"><span class="rad-list-icon pull-' +
            position +
            '"><img class="rad-list-img" src=' +
            img +
            ' alt="me" /></span><div class="rad-list-content rad-chat"><span class="lg-text">Me</span><span class="sm-text"><i class="fa fa-clock-o"></i> ' +
            formatTime(new Date()) +
            '</span><div class="rad-chat-msg">' +
            text +
            "</div>"
        );
    }

    $('.sub-menu ul').hide();
    $(".sub-menu a").click(function () {
        $(this).parent(".sub-menu").children("ul").slideToggle("100");
        $(this).find(".right").toggleClass("fa-caret-up fa-caret-down");
    });

    //Sidebar Dropdown

    $('sidebar-wrapper li').on("click", function(e) {
        e.preventDefault();
        var $item = $(".rad-dropmenu-item");
        if ($item.hasClass("active")) {
            $item.removeClass("active");
        }
    });

    $('.preload-wrapper').fadeOut(1000);

});


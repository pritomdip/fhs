$(function() {

	// Multiple Image show on preview

	// $('.file-input').on('change', function(){ //on file input change
	// 	var appendClass = $(this).siblings('.thumb-output');
	// 	appendClass.html('');

 //        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
 //        {    
 //            var data = $(this)[0].files; //this file data
            
 //            $.each(data, function(index, file){ //loop though each file
 //                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
 //                    var fRead = new FileReader(); //new filereader
 //                    fRead.onload = (function(file){ //trigger function on successful read
 //                    return function(e) {
 //                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element 
 //                        $(appendClass).append(img); //append image to output element
 //                    };
 //                    })(file);
 //                    fRead.readAsDataURL(file); //URL representing the file's data.
 //                }
 //            });   
 //        }
 //    });









    // var imagesPreview = function(input, placeToInsertImagePreview) {

    //     if (input.files) {
    //         var filesAmount = input.files.length;

    //         for (i = 0; i < filesAmount; i++) {
    //             var reader = new FileReader();

    //             reader.onload = function(event) {
    //                 $($.parseHTML('<img class="hello" style="width:80px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
    //             }

    //             reader.readAsDataURL(input.files[i]);
    //         }
    //     }

    // };

    // $('.file-input').on('change', function() {
    //     imagesPreview(this, 'div.gallery');
    // });

























	//Datepicker

    $(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		minDate: 0,
		buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
		showOn: "both",
		buttonImageOnly: true,
	});

	$(".datepicker2").datepicker({
		dateFormat: 'dd-mm-yy',
		minDate: 0,
		buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
		showOn: "both",
		buttonImageOnly: true,
	});

	// $('div.checkbox-group.required :checkbox:checked').length > 0


	// Multi Step Form

	var left, opacity, scale; //fieldset properties which we will animate
	var animating; //flag to prevent quick multi-click glitches

	var numberOfActive = $("#progressbar > li").index($("#progressbar > li.active"));
	var abc = $('fieldset').eq(numberOfActive).css({'display': 'block','left': '0%', 'opacity': '1'});

	$(".next").click(function(){
	    if(animating) return false;
	    animating = true;  
	    
		if (!$(this).siblings().find('.checkbox-group:checked').val()) {
		    alert('Please Select a ans of the question');
		    animating = false;
		    return false;
		} else {
		    $(".error-messages-answer").empty().fadeOut();
		}

		current_fs = $(this).parent();
	    next_fs = $(this).parent().next();

	    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	    
	    //activate next step on progressbar using the index of next_fs
	    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	    
	    //show the next fieldset
	    next_fs.show(); 
	    //hide the current fieldset with style
	    current_fs.animate({opacity: 0}, {
	        step: function(now, mx) {
	            //as the opacity of current_fs reduces to 0 - stored in "now"
	            //1. scale current_fs down to 80%
	            scale = 1 - (1 - now) * 0.2;
	            //2. bring next_fs from the right(50%)
	            left = (now * 50)+"%";
	            //3. increase opacity of next_fs to 1 as it moves in
	            opacity = 1 - now;
	            current_fs.css({'transform': 'scale('+scale+')'});
	            next_fs.css({'left': left, 'opacity': opacity});
	        }, 
	        duration: 500, 
	        complete: function(){
	            current_fs.hide();
	            animating = false;
	        }, 
	        //this comes from the custom easing plugin
	        easing: 'easeOutQuint'
	    });
	});

	$(".previous").click(function(){

	    if(animating) return false;
	    animating = true;
	    
	    current_fs = $(this).parent();
	    previous_fs = $(this).parent().prev();
	    
	    //de-activate current step on progressbar
	    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	    $("#progressbar li").eq($("fieldset").index(previous_fs)).addClass("active");
	    
	    //show the previous fieldset
	    previous_fs.show(); 
	    //hide the current fieldset with style
	    current_fs.animate({opacity: 0}, {
	        step: function(now, mx) {
	            //as the opacity of current_fs reduces to 0 - stored in "now"
	            //1. scale previous_fs from 80% to 100%
	            scale = 0.8 + (1 - now) * 0.2;
	            //2. take current_fs to the right(50%) - from 0%
	            left = ((1-now) * 50)+"%";
	            //3. increase opacity of previous_fs to 1 as it moves in
	            opacity = 1 - now;
	            current_fs.css({'left': left});
	            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
	        }, 
	        duration: 500, 
	        complete: function(){
	            current_fs.hide();
	            animating = false;
	        }, 
	        //this comes from the custom easing plugin
	        easing: 'easeOutQuint'
	    });
	});var current_fs, next_fs, previous_fs; //fieldsets

	$(".submit").click(function(){
	    if(animating) return false;
	    animating = true;  
	    
		if (!$(this).siblings().find('.checkbox-group:checked').val()) {
		    alert('Please Select a ans of the question');
		    animating = false;
		    return false;
		} else {
		    $(".error-messages-answer").empty().fadeOut();
		}
	});

	// Make Tr clickable

	$(document.body).on('click', '.property-table[data-href]', function(){
		window.location.href = this.dataset.href;
	});


	var maxMobileSize = 768; // this value should match the media query
  var mobileAccordionTableSelector = ".table.mobile-accordion";


  // Resize and Reorientation code borrowed and modified from this StackOverflow answer:
  //    http://stackoverflow.com/a/6603537
  var previousOrientation = window.orientation;

  var checkOrientation = function() {
      if(window.orientation !== previousOrientation){
          previousOrientation = window.orientation;
          checkScreenSize();
      }
  };

  var checkScreenSize = function() {
    var width = $(window).width();

    if (width < maxMobileSize) {
    	$('.tbl-mobile-accordion-wrapper').css('display','block');
      handleMobile();
    } else {
      handleDesktop();
    }
  }

  window.addEventListener("resize", checkScreenSize, false);
  window.addEventListener("orientationchange", checkOrientation, false);

  // Android doesn't always fire orientationChange on 180 degree turns
  setInterval(checkOrientation, 2000);


  function handleMobile() {
    slideUpAllInactive();
  }

  function handleDesktop() {
    showRows();
    addRowHighlighting();
  }

  function showRows() {
    $(mobileAccordionTableSelector + " .tr").each(function () {
      $(this).removeAttr("style");
    });
  }

  function addRowHighlighting() {
    var rows = $(".table .tbody .tr");
      
    /* Add a highlighting class on even rows */
    for (var i = 0; i < rows.length; i++) {
      if (i % 2 == 1) {
        $(rows[i]).addClass("alternate-highlight");
      }
    }
  }

  function slideUpAllInactive() {
    $(".table .rh").not(".active").each(function() {
      $(this).next().slideUp();
    });
  }

  function handleMobileAccordionTableClick(rowHeader) {
    var table = $(rowHeader).parents(".table");

    $(table).find(".rh").each(function () {
      $(this).removeClass("active");
    });

    slideUpAllInactive();

    var nextRow = $(rowHeader).next();
    if (!nextRow.is(":visible")) {
      $(rowHeader).addClass("active");
      nextRow.slideDown();
    }
  }

  $(mobileAccordionTableSelector + " .rh").click(function () {
    handleMobileAccordionTableClick(this);
  });

  // Need to run this on first load
  checkScreenSize();





  // Deleting image

  $('.image-delete').on('click', function (e){

  	e.preventDefault();

  	var thisImge	= $(this);
  	var imgUrl 		= $(this).siblings('.evidence-imgs').attr('attribute');
  	var questionId 	= $(this).parents('.content-wrapper').find('.question_id').val();
  	var propertyId 	= $(this).parents('.question-wrapper').attr('property_id');
  	var lang 		= $(this).attr('lang');

  	var data = {
  		'image_url'   : imgUrl,
  		'question_id' : questionId,
  		'property_id' : propertyId
  	};

  	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  	$.ajax({
	    url: "/" + lang + "/property/" + propertyId + "/question/" + questionId ,
	    method: 'POST',
	    data:  data,
	    success: function(data) {
	        thisImge.parent('.gallery-items').hide();
	    },
	    error: function(data) {                 
	    }
	});



  });



  $(".select-multiple").select2();






});
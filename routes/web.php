<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin')->group(function(){
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/login', 'Auth\Admin\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login/submit', 'Auth\Admin\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\Admin\AdminLoginController@adminLogout')->name('admin.logout');

    Route::get('/profile/{admin}', 'AdminController@adminProfile')->name('admin.profile');
    Route::post('/profile/{admin}', 'AdminController@changePassword')->name('admin.change.password');

    // For Admin Password Reset
    Route::post('/password/email', 'Auth\Admin\AdminForgotPasswordController@sendResetLinkEmail' )->name('admin.password.email');
	Route::get('/password/reset', 'Auth\Admin\AdminForgotPasswordController@showLinkRequestForm' )->name('admin.password.update');
	Route::post('/password/reset', 'Auth\Admin\AdminResetPasswordController@reset' );
	Route::get('/password/reset/{token}', 'Auth\Admin\AdminResetPasswordController@showResetForm' )->name('admin.password.reset');


	//User CRUD on admin

	Route::prefix('/users')->group(function(){
		Route::get('/list', 'Admin\UsersController@index')->name('admin.users.list');
		Route::get('/add', 'Admin\UsersController@show')->name('admin.users.add');
		Route::post('/submit', 'Admin\UsersController@store')->name('admin.user.submit');
		Route::get('/edit/{id}', 'Admin\UsersController@edit')->name('admin.users.edit');
		Route::post('/update/{id}', 'Admin\UsersController@update')->name('admin.users.update');
		Route::get('/delete/{id}', 'Admin\UsersController@destroy')->name('admin.users.delete');
	});

	// Property Crud

	Route::prefix('/property')->group(function(){
		Route::get('/list', 'Admin\PropertyController@index')->name('admin.property.list');
		Route::get('/add', 'Admin\PropertyController@show')->name('admin.property.add');
		Route::post('/submit', 'Admin\PropertyController@store')->name('admin.property.submit');
		Route::get('/edit/{id}', 'Admin\PropertyController@edit')->name('admin.property.edit');
		Route::post('/update/{id}', 'Admin\PropertyController@update')->name('admin.property.update');
		Route::get('/delete/{id}', 'Admin\PropertyController@destroy')->name('admin.property.delete');
	});

	// Category Crud

	Route::prefix('/category')->group(function(){
		Route::get('/list', 'Admin\QuesCategoryController@index')->name('admin.category.list');
		Route::get('/add', 'Admin\QuesCategoryController@show')->name('admin.category.add');
		Route::post('/submit', 'Admin\QuesCategoryController@store')->name('admin.category.submit');
		Route::get('/edit/{id}', 'Admin\QuesCategoryController@edit')->name('admin.category.edit');
		Route::post('/update/{id}', 'Admin\QuesCategoryController@update')->name('admin.category.update');
		Route::get('/delete/{id}', 'Admin\QuesCategoryController@destroy')->name('admin.category.delete');
	});

	// Question Crud

	Route::prefix('/question')->group(function(){
		Route::get('/list', 'Admin\QuestionController@index')->name('admin.question.list');
		Route::get('/add', 'Admin\QuestionController@show')->name('admin.question.add');
		Route::post('/submit', 'Admin\QuestionController@store')->name('admin.question.submit');
		Route::get('/edit/{id}', 'Admin\QuestionController@edit')->name('admin.question.edit');
		Route::post('/update/{id}', 'Admin\QuestionController@update')->name('admin.question.update');
		Route::get('/delete/{id}', 'Admin\QuestionController@destroy')->name('admin.question.delete');
	});

	// Terms CRUD

	Route::resource('term', 'Admin\TermController');

});

// Frontend Routes

Route::redirect('/', app()->getLocale() . '/login');

Route::group(["prefix" => "{language}"], function(){
	Route::get('/', 'HomeController@home')->name('home');
	Route::get('/properties/{term}', 'HomeController@index')->name('property.term');
	Route::get('/property/{id}', 'HomeController@propertyQuestions')->name('property');
	Route::get('/property/{id}/question', 'HomeController@questions')->name('question');
	Route::post('/answers/submit/{property}', 'HomeController@answers')->name('answers.submit');
	Route::post('/property/{property}/question/{question}', 'HomeController@deleteImage');	
});

Route::group(["prefix" => "{language?}"], function(){
	Auth::routes(['register' => false]);
});